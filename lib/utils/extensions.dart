import 'package:easy_localization/easy_localization.dart';

extension Currency on num {
  toCurrency() =>
      NumberFormat.currency(locale: 'nl_NL', symbol: '€').format(this);

  toCurrencyWithoutSign() =>
      NumberFormat.currency(locale: 'nl_NL').format(this);
}
