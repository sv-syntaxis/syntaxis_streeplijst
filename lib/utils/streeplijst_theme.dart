import 'package:flutter/material.dart';

class StreeplijstTheme {
  static final _primary = createMaterialColor(Color(0xFF7A7A7A));
  static final _secondary = createMaterialColor(Color(0xFF77C145));

  static ThemeData getTheme(BuildContext context) => ThemeData(
        colorScheme: ColorScheme(
          primary: _primary,
          onSecondary: Colors.white,
          brightness: Brightness.light,
          onError: Colors.white,
          onBackground: Colors.black,
          error: Color(0xFFD62828),
          surface: Colors.white,
          secondary: _secondary,
          onPrimary: Colors.white,
          background: Colors.white,
          onSurface: Colors.black,
        ),
        textTheme: TextTheme(
          headlineLarge: TextStyle(
            color: _primary.shade900,
            fontSize: 33,
            fontWeight: FontWeight.w500,
            height: 51 / 33,
            letterSpacing: 1,
          ),
          headlineMedium: TextStyle(
            color: _primary.shade900,
            fontSize: 26,
            fontWeight: FontWeight.w500,
            height: 41 / 26,
            letterSpacing: 1,
          ),
          headlineSmall: TextStyle(
            color: _primary.shade900,
            fontSize: 20,
            fontWeight: FontWeight.w500,
            height: 33 / 20,
            letterSpacing: 1,
          ),
          bodyMedium: TextStyle(
            color: _primary.shade900,
            fontSize: 16,
            fontWeight: FontWeight.w400,
            height: 27 / 16,
            letterSpacing: 1,
          ),
          bodySmall: TextStyle(
            color: _primary.shade900,
            fontSize: 16,
            fontWeight: FontWeight.w300,
            height: 27 / 16,
            letterSpacing: 1,
          ),
          titleMedium: TextStyle(
            color: _primary.shade900,
            fontSize: 20,
            fontWeight: FontWeight.w300,
            height: 33 / 20,
            letterSpacing: 1,
          ),
          titleLarge: TextStyle(
            color: _primary.shade900,
            fontSize: 33,
            fontWeight: FontWeight.w300,
            height: 51 / 33,
            letterSpacing: 1,
          ),
          titleSmall: TextStyle(
            color: _primary.shade700,
            fontSize: 13,
            fontWeight: FontWeight.w300,
            height: 13 / 23,
            letterSpacing: 1,
          ),
        ),
        elevatedButtonTheme: ElevatedButtonThemeData(
          style: ButtonStyle(
            shape: MaterialStateProperty.all(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(16.0),
              ),
            ),
            fixedSize: MaterialStateProperty.all(Size(200, 70)),
            minimumSize: MaterialStateProperty.all(Size(200, 70)),
            maximumSize: MaterialStateProperty.all(Size(200, 70)),
          ),
        ),
        outlinedButtonTheme: OutlinedButtonThemeData(
          style: ButtonStyle(
            shape: MaterialStateProperty.all(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(16.0),
              ),
            ),
            fixedSize: MaterialStateProperty.all(Size(200, 70)),
            minimumSize: MaterialStateProperty.all(Size(200, 70)),
            maximumSize: MaterialStateProperty.all(Size(200, 70)),
          ),
        ),
      );

  /// Creates a [MaterialColor] based on the supplied [Color]
  static MaterialColor createMaterialColor(Color color) {
    final strengths = <double>[.05];
    final swatch = <int, Color>{};
    final r = color.red, g = color.green, b = color.blue;

    for (var i = 1; i < 10; i++) {
      strengths.add(0.1 * i);
    }
    for (final strength in strengths) {
      final ds = 0.5 - strength;
      swatch[(strength * 1000).round()] = Color.fromRGBO(
        r + ((ds < 0 ? r : (255 - r)) * ds).round(),
        g + ((ds < 0 ? g : (255 - g)) * ds).round(),
        b + ((ds < 0 ? b : (255 - b)) * ds).round(),
        1,
      );
    }
    return MaterialColor(color.value, swatch);
  }
}
