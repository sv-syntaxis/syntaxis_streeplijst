import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';

/// Class for handling requests
class RequestHandler {
  /// Method that executes [request] and returns the value it produces.
  ///
  /// When [request] produces a [DioError] it will check if the status code
  /// matches any of the given values in [onStatusCodeErrorMap] and it will
  /// return the corresponding value.
  /// If the status code is not found in [onStatusCodeErrorMap] it will return
  /// [defaultOnStatusCodeError]
  static Future<T> request<T>(
      {required Future<T> request,
      Map<int, T>? onStatusCodeErrorMap,
      required T defaultOnStatusCodeError}) async {
    try {
      final result = await request;
      debugPrint('RequestHandler response ${result.toString()}');
      return result;
    } on DioError catch (e) {
      if (e.response != null && onStatusCodeErrorMap != null) {
        debugPrint('RequestHandler DioException: ${e.toString()}');
        if (onStatusCodeErrorMap.containsKey(e.response?.statusCode)) {
          return onStatusCodeErrorMap[e.response?.statusCode]!;
        } else {
          return defaultOnStatusCodeError;
        }
      } else {
        debugPrint('RequestHandler DioException: ${e.toString()}');
        rethrow;
      }
    }
  }
}
