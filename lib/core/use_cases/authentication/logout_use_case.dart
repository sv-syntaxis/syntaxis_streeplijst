import '../../providers/authentication/authentication_provider.dart';

class LogoutUseCase {
  final AuthenticationProvider _authenticationProvider;

  LogoutUseCase(this._authenticationProvider);

  Future<void> call() async {
    return _authenticationProvider.logout();
  }
}
