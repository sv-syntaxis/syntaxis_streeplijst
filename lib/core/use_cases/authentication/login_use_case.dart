import '../../providers/authentication/authentication_provider.dart';

class LoginUseCase {
  final AuthenticationProvider _authenticationProvider;

  LoginUseCase(this._authenticationProvider);

  Future<void> call(String barcode) async {
    return _authenticationProvider.login(barcode);
  }
}
