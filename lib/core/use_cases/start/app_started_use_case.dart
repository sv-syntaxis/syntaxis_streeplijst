import '../../entities/user/user.dart';
import '../../providers/authentication/authentication_provider.dart';

class AppStartedUseCase {
  final AuthenticationProvider _authenticationProvider;

  AppStartedUseCase(this._authenticationProvider);

  Stream<User?> get authStatus => _authenticationProvider.authStatus;
}
