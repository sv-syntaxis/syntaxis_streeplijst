import '../../providers/authentication/authentication_provider.dart';

import '../../repositories/user/user_repository.dart';

class GetUserBalanceUseCase {
  final UserRepository _userRepository;
  final AuthenticationProvider _authenticationProvider;

  GetUserBalanceUseCase(this._userRepository, this._authenticationProvider);

  Future<double> call() async {
    return _userRepository.getUserBalance(_authenticationProvider.user!.id);
  }
}
