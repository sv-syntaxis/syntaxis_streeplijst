import '../../entities/product/product.dart';
import '../../repositories/products/products_repository.dart';

class LoadProductsByCategoryIdUseCase {
  final ProductsRepository _productRepository;

  LoadProductsByCategoryIdUseCase(this._productRepository);

  Future<List<Product>> call(int id) {
    return _productRepository.getProductByCategoryId(id);
  }
}
