import '../../entities/category/category.dart';
import '../../repositories/categories/categories_repository.dart';

class GetCategoriesUseCase {
  final CategoriesRepository _categoryRepository;

  GetCategoriesUseCase(this._categoryRepository);
  Future<List<Category>> call() {
    return _categoryRepository.getCategories();
  }
}
