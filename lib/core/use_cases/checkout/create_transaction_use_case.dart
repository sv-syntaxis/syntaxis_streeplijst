import '../../entities/transaction/transaction.dart';
import '../../repositories/transaction/transaction_repository.dart';

class CreateTransactionUseCase {
  final TransactionsRepository _repository;

  CreateTransactionUseCase(this._repository);

  Future<void> call(TransactionWrite transaction) {
    return _repository.addTransaction(transaction);
  }
}
