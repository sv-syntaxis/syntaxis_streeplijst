// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'transaction_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$TransactionItemWriteImpl _$$TransactionItemWriteImplFromJson(
        Map<String, dynamic> json) =>
    _$TransactionItemWriteImpl(
      amount: json['amount'] as int,
      product: json['product'] as int,
    );

Map<String, dynamic> _$$TransactionItemWriteImplToJson(
        _$TransactionItemWriteImpl instance) =>
    <String, dynamic>{
      'amount': instance.amount,
      'product': instance.product,
    };

_$TransactionItemReadImpl _$$TransactionItemReadImplFromJson(
        Map<String, dynamic> json) =>
    _$TransactionItemReadImpl(
      balanceChange: (json['balanceChange'] as num).toDouble(),
      refunded: json['refunded'] as bool,
      product: Product.fromJson(json['product'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$TransactionItemReadImplToJson(
        _$TransactionItemReadImpl instance) =>
    <String, dynamic>{
      'balanceChange': instance.balanceChange,
      'refunded': instance.refunded,
      'product': instance.product,
    };
