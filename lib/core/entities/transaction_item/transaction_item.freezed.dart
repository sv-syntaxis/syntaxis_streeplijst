// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'transaction_item.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

TransactionItemWrite _$TransactionItemWriteFromJson(Map<String, dynamic> json) {
  return _TransactionItemWrite.fromJson(json);
}

/// @nodoc
mixin _$TransactionItemWrite {
  int get amount => throw _privateConstructorUsedError;
  int get product => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $TransactionItemWriteCopyWith<TransactionItemWrite> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TransactionItemWriteCopyWith<$Res> {
  factory $TransactionItemWriteCopyWith(TransactionItemWrite value,
          $Res Function(TransactionItemWrite) then) =
      _$TransactionItemWriteCopyWithImpl<$Res, TransactionItemWrite>;
  @useResult
  $Res call({int amount, int product});
}

/// @nodoc
class _$TransactionItemWriteCopyWithImpl<$Res,
        $Val extends TransactionItemWrite>
    implements $TransactionItemWriteCopyWith<$Res> {
  _$TransactionItemWriteCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? amount = null,
    Object? product = null,
  }) {
    return _then(_value.copyWith(
      amount: null == amount
          ? _value.amount
          : amount // ignore: cast_nullable_to_non_nullable
              as int,
      product: null == product
          ? _value.product
          : product // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$TransactionItemWriteImplCopyWith<$Res>
    implements $TransactionItemWriteCopyWith<$Res> {
  factory _$$TransactionItemWriteImplCopyWith(_$TransactionItemWriteImpl value,
          $Res Function(_$TransactionItemWriteImpl) then) =
      __$$TransactionItemWriteImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({int amount, int product});
}

/// @nodoc
class __$$TransactionItemWriteImplCopyWithImpl<$Res>
    extends _$TransactionItemWriteCopyWithImpl<$Res, _$TransactionItemWriteImpl>
    implements _$$TransactionItemWriteImplCopyWith<$Res> {
  __$$TransactionItemWriteImplCopyWithImpl(_$TransactionItemWriteImpl _value,
      $Res Function(_$TransactionItemWriteImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? amount = null,
    Object? product = null,
  }) {
    return _then(_$TransactionItemWriteImpl(
      amount: null == amount
          ? _value.amount
          : amount // ignore: cast_nullable_to_non_nullable
              as int,
      product: null == product
          ? _value.product
          : product // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$TransactionItemWriteImpl implements _TransactionItemWrite {
  const _$TransactionItemWriteImpl(
      {required this.amount, required this.product});

  factory _$TransactionItemWriteImpl.fromJson(Map<String, dynamic> json) =>
      _$$TransactionItemWriteImplFromJson(json);

  @override
  final int amount;
  @override
  final int product;

  @override
  String toString() {
    return 'TransactionItemWrite(amount: $amount, product: $product)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$TransactionItemWriteImpl &&
            (identical(other.amount, amount) || other.amount == amount) &&
            (identical(other.product, product) || other.product == product));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, amount, product);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$TransactionItemWriteImplCopyWith<_$TransactionItemWriteImpl>
      get copyWith =>
          __$$TransactionItemWriteImplCopyWithImpl<_$TransactionItemWriteImpl>(
              this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$TransactionItemWriteImplToJson(
      this,
    );
  }
}

abstract class _TransactionItemWrite implements TransactionItemWrite {
  const factory _TransactionItemWrite(
      {required final int amount,
      required final int product}) = _$TransactionItemWriteImpl;

  factory _TransactionItemWrite.fromJson(Map<String, dynamic> json) =
      _$TransactionItemWriteImpl.fromJson;

  @override
  int get amount;
  @override
  int get product;
  @override
  @JsonKey(ignore: true)
  _$$TransactionItemWriteImplCopyWith<_$TransactionItemWriteImpl>
      get copyWith => throw _privateConstructorUsedError;
}

TransactionItemRead _$TransactionItemReadFromJson(Map<String, dynamic> json) {
  return _TransactionItemRead.fromJson(json);
}

/// @nodoc
mixin _$TransactionItemRead {
  double get balanceChange => throw _privateConstructorUsedError;
  bool get refunded => throw _privateConstructorUsedError;
  Product get product => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $TransactionItemReadCopyWith<TransactionItemRead> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TransactionItemReadCopyWith<$Res> {
  factory $TransactionItemReadCopyWith(
          TransactionItemRead value, $Res Function(TransactionItemRead) then) =
      _$TransactionItemReadCopyWithImpl<$Res, TransactionItemRead>;
  @useResult
  $Res call({double balanceChange, bool refunded, Product product});

  $ProductCopyWith<$Res> get product;
}

/// @nodoc
class _$TransactionItemReadCopyWithImpl<$Res, $Val extends TransactionItemRead>
    implements $TransactionItemReadCopyWith<$Res> {
  _$TransactionItemReadCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? balanceChange = null,
    Object? refunded = null,
    Object? product = null,
  }) {
    return _then(_value.copyWith(
      balanceChange: null == balanceChange
          ? _value.balanceChange
          : balanceChange // ignore: cast_nullable_to_non_nullable
              as double,
      refunded: null == refunded
          ? _value.refunded
          : refunded // ignore: cast_nullable_to_non_nullable
              as bool,
      product: null == product
          ? _value.product
          : product // ignore: cast_nullable_to_non_nullable
              as Product,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $ProductCopyWith<$Res> get product {
    return $ProductCopyWith<$Res>(_value.product, (value) {
      return _then(_value.copyWith(product: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$TransactionItemReadImplCopyWith<$Res>
    implements $TransactionItemReadCopyWith<$Res> {
  factory _$$TransactionItemReadImplCopyWith(_$TransactionItemReadImpl value,
          $Res Function(_$TransactionItemReadImpl) then) =
      __$$TransactionItemReadImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({double balanceChange, bool refunded, Product product});

  @override
  $ProductCopyWith<$Res> get product;
}

/// @nodoc
class __$$TransactionItemReadImplCopyWithImpl<$Res>
    extends _$TransactionItemReadCopyWithImpl<$Res, _$TransactionItemReadImpl>
    implements _$$TransactionItemReadImplCopyWith<$Res> {
  __$$TransactionItemReadImplCopyWithImpl(_$TransactionItemReadImpl _value,
      $Res Function(_$TransactionItemReadImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? balanceChange = null,
    Object? refunded = null,
    Object? product = null,
  }) {
    return _then(_$TransactionItemReadImpl(
      balanceChange: null == balanceChange
          ? _value.balanceChange
          : balanceChange // ignore: cast_nullable_to_non_nullable
              as double,
      refunded: null == refunded
          ? _value.refunded
          : refunded // ignore: cast_nullable_to_non_nullable
              as bool,
      product: null == product
          ? _value.product
          : product // ignore: cast_nullable_to_non_nullable
              as Product,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$TransactionItemReadImpl implements _TransactionItemRead {
  const _$TransactionItemReadImpl(
      {required this.balanceChange,
      required this.refunded,
      required this.product});

  factory _$TransactionItemReadImpl.fromJson(Map<String, dynamic> json) =>
      _$$TransactionItemReadImplFromJson(json);

  @override
  final double balanceChange;
  @override
  final bool refunded;
  @override
  final Product product;

  @override
  String toString() {
    return 'TransactionItemRead(balanceChange: $balanceChange, refunded: $refunded, product: $product)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$TransactionItemReadImpl &&
            (identical(other.balanceChange, balanceChange) ||
                other.balanceChange == balanceChange) &&
            (identical(other.refunded, refunded) ||
                other.refunded == refunded) &&
            (identical(other.product, product) || other.product == product));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode =>
      Object.hash(runtimeType, balanceChange, refunded, product);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$TransactionItemReadImplCopyWith<_$TransactionItemReadImpl> get copyWith =>
      __$$TransactionItemReadImplCopyWithImpl<_$TransactionItemReadImpl>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$TransactionItemReadImplToJson(
      this,
    );
  }
}

abstract class _TransactionItemRead implements TransactionItemRead {
  const factory _TransactionItemRead(
      {required final double balanceChange,
      required final bool refunded,
      required final Product product}) = _$TransactionItemReadImpl;

  factory _TransactionItemRead.fromJson(Map<String, dynamic> json) =
      _$TransactionItemReadImpl.fromJson;

  @override
  double get balanceChange;
  @override
  bool get refunded;
  @override
  Product get product;
  @override
  @JsonKey(ignore: true)
  _$$TransactionItemReadImplCopyWith<_$TransactionItemReadImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
