import 'package:freezed_annotation/freezed_annotation.dart';

import '../image/image.dart';

part 'product.freezed.dart';
part 'product.g.dart';

@freezed
class Product with _$Product {
  const Product._();

  const factory Product({
    required int id,
    required double price,
    required int discountPercentage,
    required String name,
    required double deposit,
    Image? image,
  }) = _Product;

  bool get hasDiscount => (discountPercentage ?? 0) > 0;

  double get _discount => (discountPercentage ?? 0) / 100;

  bool get hasDeposit => deposit > 0;

  double get totalPriceWithDiscount => price * (1 - _discount) + deposit;

  double get totalWithoutDiscount => price + deposit;

  double get discountAmount => price - (price * (1 - _discount));

  factory Product.fromJson(Map<String, dynamic> json) =>
      _$ProductFromJson(json);
}

enum DiscountType { NONE, SINGLE }
