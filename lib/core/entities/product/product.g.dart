// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$ProductImpl _$$ProductImplFromJson(Map<String, dynamic> json) =>
    _$ProductImpl(
      id: json['id'] as int,
      price: (json['price'] as num).toDouble(),
      discountPercentage: json['discountPercentage'] as int,
      name: json['name'] as String,
      deposit: (json['deposit'] as num).toDouble(),
      image: json['image'] == null
          ? null
          : Image.fromJson(json['image'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$ProductImplToJson(_$ProductImpl instance) =>
    <String, dynamic>{
      'id': instance.id,
      'price': instance.price,
      'discountPercentage': instance.discountPercentage,
      'name': instance.name,
      'deposit': instance.deposit,
      'image': instance.image,
    };
