// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'transaction.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

TransactionWrite _$TransactionWriteFromJson(Map<String, dynamic> json) {
  return _TransactionWrite.fromJson(json);
}

/// @nodoc
mixin _$TransactionWrite {
  List<TransactionItemWrite> get transactionItems =>
      throw _privateConstructorUsedError;
  int get userId => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $TransactionWriteCopyWith<TransactionWrite> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TransactionWriteCopyWith<$Res> {
  factory $TransactionWriteCopyWith(
          TransactionWrite value, $Res Function(TransactionWrite) then) =
      _$TransactionWriteCopyWithImpl<$Res, TransactionWrite>;
  @useResult
  $Res call({List<TransactionItemWrite> transactionItems, int userId});
}

/// @nodoc
class _$TransactionWriteCopyWithImpl<$Res, $Val extends TransactionWrite>
    implements $TransactionWriteCopyWith<$Res> {
  _$TransactionWriteCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? transactionItems = null,
    Object? userId = null,
  }) {
    return _then(_value.copyWith(
      transactionItems: null == transactionItems
          ? _value.transactionItems
          : transactionItems // ignore: cast_nullable_to_non_nullable
              as List<TransactionItemWrite>,
      userId: null == userId
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$TransactionWriteImplCopyWith<$Res>
    implements $TransactionWriteCopyWith<$Res> {
  factory _$$TransactionWriteImplCopyWith(_$TransactionWriteImpl value,
          $Res Function(_$TransactionWriteImpl) then) =
      __$$TransactionWriteImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({List<TransactionItemWrite> transactionItems, int userId});
}

/// @nodoc
class __$$TransactionWriteImplCopyWithImpl<$Res>
    extends _$TransactionWriteCopyWithImpl<$Res, _$TransactionWriteImpl>
    implements _$$TransactionWriteImplCopyWith<$Res> {
  __$$TransactionWriteImplCopyWithImpl(_$TransactionWriteImpl _value,
      $Res Function(_$TransactionWriteImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? transactionItems = null,
    Object? userId = null,
  }) {
    return _then(_$TransactionWriteImpl(
      transactionItems: null == transactionItems
          ? _value._transactionItems
          : transactionItems // ignore: cast_nullable_to_non_nullable
              as List<TransactionItemWrite>,
      userId: null == userId
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$TransactionWriteImpl implements _TransactionWrite {
  const _$TransactionWriteImpl(
      {required final List<TransactionItemWrite> transactionItems,
      required this.userId})
      : _transactionItems = transactionItems;

  factory _$TransactionWriteImpl.fromJson(Map<String, dynamic> json) =>
      _$$TransactionWriteImplFromJson(json);

  final List<TransactionItemWrite> _transactionItems;
  @override
  List<TransactionItemWrite> get transactionItems {
    if (_transactionItems is EqualUnmodifiableListView)
      return _transactionItems;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_transactionItems);
  }

  @override
  final int userId;

  @override
  String toString() {
    return 'TransactionWrite(transactionItems: $transactionItems, userId: $userId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$TransactionWriteImpl &&
            const DeepCollectionEquality()
                .equals(other._transactionItems, _transactionItems) &&
            (identical(other.userId, userId) || other.userId == userId));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType,
      const DeepCollectionEquality().hash(_transactionItems), userId);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$TransactionWriteImplCopyWith<_$TransactionWriteImpl> get copyWith =>
      __$$TransactionWriteImplCopyWithImpl<_$TransactionWriteImpl>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$TransactionWriteImplToJson(
      this,
    );
  }
}

abstract class _TransactionWrite implements TransactionWrite {
  const factory _TransactionWrite(
      {required final List<TransactionItemWrite> transactionItems,
      required final int userId}) = _$TransactionWriteImpl;

  factory _TransactionWrite.fromJson(Map<String, dynamic> json) =
      _$TransactionWriteImpl.fromJson;

  @override
  List<TransactionItemWrite> get transactionItems;
  @override
  int get userId;
  @override
  @JsonKey(ignore: true)
  _$$TransactionWriteImplCopyWith<_$TransactionWriteImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

TransactionRead _$TransactionReadFromJson(Map<String, dynamic> json) {
  return _TransactionRead.fromJson(json);
}

/// @nodoc
mixin _$TransactionRead {
  int? get id => throw _privateConstructorUsedError;
  double? get balanceChange => throw _privateConstructorUsedError;
  DateTime get createdOn => throw _privateConstructorUsedError;
  List<TransactionItemRead> get transactionItems =>
      throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $TransactionReadCopyWith<TransactionRead> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TransactionReadCopyWith<$Res> {
  factory $TransactionReadCopyWith(
          TransactionRead value, $Res Function(TransactionRead) then) =
      _$TransactionReadCopyWithImpl<$Res, TransactionRead>;
  @useResult
  $Res call(
      {int? id,
      double? balanceChange,
      DateTime createdOn,
      List<TransactionItemRead> transactionItems});
}

/// @nodoc
class _$TransactionReadCopyWithImpl<$Res, $Val extends TransactionRead>
    implements $TransactionReadCopyWith<$Res> {
  _$TransactionReadCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? balanceChange = freezed,
    Object? createdOn = null,
    Object? transactionItems = null,
  }) {
    return _then(_value.copyWith(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      balanceChange: freezed == balanceChange
          ? _value.balanceChange
          : balanceChange // ignore: cast_nullable_to_non_nullable
              as double?,
      createdOn: null == createdOn
          ? _value.createdOn
          : createdOn // ignore: cast_nullable_to_non_nullable
              as DateTime,
      transactionItems: null == transactionItems
          ? _value.transactionItems
          : transactionItems // ignore: cast_nullable_to_non_nullable
              as List<TransactionItemRead>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$TransactionReadImplCopyWith<$Res>
    implements $TransactionReadCopyWith<$Res> {
  factory _$$TransactionReadImplCopyWith(_$TransactionReadImpl value,
          $Res Function(_$TransactionReadImpl) then) =
      __$$TransactionReadImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int? id,
      double? balanceChange,
      DateTime createdOn,
      List<TransactionItemRead> transactionItems});
}

/// @nodoc
class __$$TransactionReadImplCopyWithImpl<$Res>
    extends _$TransactionReadCopyWithImpl<$Res, _$TransactionReadImpl>
    implements _$$TransactionReadImplCopyWith<$Res> {
  __$$TransactionReadImplCopyWithImpl(
      _$TransactionReadImpl _value, $Res Function(_$TransactionReadImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? balanceChange = freezed,
    Object? createdOn = null,
    Object? transactionItems = null,
  }) {
    return _then(_$TransactionReadImpl(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      balanceChange: freezed == balanceChange
          ? _value.balanceChange
          : balanceChange // ignore: cast_nullable_to_non_nullable
              as double?,
      createdOn: null == createdOn
          ? _value.createdOn
          : createdOn // ignore: cast_nullable_to_non_nullable
              as DateTime,
      transactionItems: null == transactionItems
          ? _value._transactionItems
          : transactionItems // ignore: cast_nullable_to_non_nullable
              as List<TransactionItemRead>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$TransactionReadImpl implements _TransactionRead {
  const _$TransactionReadImpl(
      {required this.id,
      required this.balanceChange,
      required this.createdOn,
      required final List<TransactionItemRead> transactionItems})
      : _transactionItems = transactionItems;

  factory _$TransactionReadImpl.fromJson(Map<String, dynamic> json) =>
      _$$TransactionReadImplFromJson(json);

  @override
  final int? id;
  @override
  final double? balanceChange;
  @override
  final DateTime createdOn;
  final List<TransactionItemRead> _transactionItems;
  @override
  List<TransactionItemRead> get transactionItems {
    if (_transactionItems is EqualUnmodifiableListView)
      return _transactionItems;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_transactionItems);
  }

  @override
  String toString() {
    return 'TransactionRead(id: $id, balanceChange: $balanceChange, createdOn: $createdOn, transactionItems: $transactionItems)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$TransactionReadImpl &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.balanceChange, balanceChange) ||
                other.balanceChange == balanceChange) &&
            (identical(other.createdOn, createdOn) ||
                other.createdOn == createdOn) &&
            const DeepCollectionEquality()
                .equals(other._transactionItems, _transactionItems));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, id, balanceChange, createdOn,
      const DeepCollectionEquality().hash(_transactionItems));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$TransactionReadImplCopyWith<_$TransactionReadImpl> get copyWith =>
      __$$TransactionReadImplCopyWithImpl<_$TransactionReadImpl>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$TransactionReadImplToJson(
      this,
    );
  }
}

abstract class _TransactionRead implements TransactionRead {
  const factory _TransactionRead(
          {required final int? id,
          required final double? balanceChange,
          required final DateTime createdOn,
          required final List<TransactionItemRead> transactionItems}) =
      _$TransactionReadImpl;

  factory _TransactionRead.fromJson(Map<String, dynamic> json) =
      _$TransactionReadImpl.fromJson;

  @override
  int? get id;
  @override
  double? get balanceChange;
  @override
  DateTime get createdOn;
  @override
  List<TransactionItemRead> get transactionItems;
  @override
  @JsonKey(ignore: true)
  _$$TransactionReadImplCopyWith<_$TransactionReadImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
