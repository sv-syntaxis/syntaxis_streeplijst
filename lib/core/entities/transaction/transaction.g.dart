// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'transaction.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$TransactionWriteImpl _$$TransactionWriteImplFromJson(
        Map<String, dynamic> json) =>
    _$TransactionWriteImpl(
      transactionItems: (json['transactionItems'] as List<dynamic>)
          .map((e) => TransactionItemWrite.fromJson(e as Map<String, dynamic>))
          .toList(),
      userId: json['userId'] as int,
    );

Map<String, dynamic> _$$TransactionWriteImplToJson(
        _$TransactionWriteImpl instance) =>
    <String, dynamic>{
      'transactionItems': instance.transactionItems,
      'userId': instance.userId,
    };

_$TransactionReadImpl _$$TransactionReadImplFromJson(
        Map<String, dynamic> json) =>
    _$TransactionReadImpl(
      id: json['id'] as int?,
      balanceChange: (json['balanceChange'] as num?)?.toDouble(),
      createdOn: DateTime.parse(json['createdOn'] as String),
      transactionItems: (json['transactionItems'] as List<dynamic>)
          .map((e) => TransactionItemRead.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$TransactionReadImplToJson(
        _$TransactionReadImpl instance) =>
    <String, dynamic>{
      'id': instance.id,
      'balanceChange': instance.balanceChange,
      'createdOn': instance.createdOn.toIso8601String(),
      'transactionItems': instance.transactionItems,
    };
