import 'dart:async';

import '../../entities/token/token.dart';
import '../../entities/user/user.dart';
import '../../repositories/login/login_repository.dart';

class AuthenticationProvider {
  final LoginRepository _loginRepository;
  final StreamController<User?> _authStatusStreamController =
      StreamController<User?>();

  Stream<User?> get authStatus => _authStatusStreamController.stream;

  User? get user => _token?.user;

  String? get refreshToken => _token?.refreshToken;

  String? get accessToken => _token?.accessToken;

  AuthenticationProvider(this._loginRepository) {
    _token = null;
  }

  Token? _token;

  Future<void> login(String userCode) async {
    final token = await _loginRepository.login(userCode);
    if (token != null) {
      _token = token;
      _authStatusStreamController.add(_token!.user);
    }
  }

  Future<void> logout() async {
    _token = null;

    _authStatusStreamController.sink.add(null);
  }
}
