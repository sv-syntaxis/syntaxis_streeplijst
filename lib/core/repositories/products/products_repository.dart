import '../../entities/product/product.dart';

mixin ProductsRepository {
  Future<List<Product>> getProductByCategoryId(int categoryId);
  Future<List<Product>> getProducts();
}
