import '../../entities/token/token.dart';

mixin LoginRepository {
  Future<Token?> login(String barcode);
}
