import '../../entities/user/user.dart';

mixin UserRepository {
  Future<double> getUserBalance(int userId);
}
