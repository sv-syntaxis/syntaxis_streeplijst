import '../../../core/repositories/user/user_repository.dart';

class MockUserRepository implements UserRepository {
  @override
  Future<double> getUserBalance(int userId) async {
    await Future.delayed(Duration(milliseconds: 150));
    return 123.12;
  }
}
