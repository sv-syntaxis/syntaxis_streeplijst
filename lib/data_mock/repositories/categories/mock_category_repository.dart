import '../../../core/entities/category/category.dart';
import '../../../core/repositories/categories/categories_repository.dart';

class MockCategoryRepository implements CategoriesRepository {
  @override
  Future<List<Category>> getCategories() async {
    await Future.delayed(Duration(milliseconds: 150));
    return [
      Category(
        id: 1,
        name: 'Drinken',
      ),
      Category(
        id: 2,
        name: 'Snacks',
      ),
      Category(
        id: 3,
        name: 'LAN Party',
      ),
      Category(
        id: 4,
        name: 'Merchandise',
      ),
    ];
  }
}
