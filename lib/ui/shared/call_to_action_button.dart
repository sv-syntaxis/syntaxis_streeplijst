import 'package:flutter/material.dart';
import '../../utils/material_state_value_builder.dart';

class CallToActionButton extends StatelessWidget {
  final Color? backgroundColor;
  final Color? textColor;
  final String text;
  final VoidCallback? onPressed;

  final Color defaultTextColor = Colors.white;

  CallToActionButton({
    required this.text,
    required this.onPressed,
    this.backgroundColor,
    this.textColor,
  }) : super();

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      child: Text(
        text,
        textAlign: TextAlign.start,
        style: Theme.of(context)
            .textTheme
            .headlineSmall!
            .copyWith(color: textColor ?? defaultTextColor),
      ),
      onPressed: onPressed,
      style: ElevatedButtonTheme.of(context).style!.copyWith(
            backgroundColor: MaterialStateValueBuilder(
              defaultValue: backgroundColor ?? Theme.of(context).primaryColor,
              disabledValue: Color(0xFF999999),
            ),
          ),
    );
  }
}
