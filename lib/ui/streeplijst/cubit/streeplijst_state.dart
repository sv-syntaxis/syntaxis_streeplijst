import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../core/entities/category/category.dart';
import '../../../core/entities/product/product.dart';

part 'streeplijst_state.freezed.dart';

@freezed
class StreeplijstState with _$StreeplijstState {
  const StreeplijstState._();

  const factory StreeplijstState({
    required double balance,
    required int categoryIndex,
    List<Category>? categories,
    List<Product>? products,
    @Default(false) bool isCategoriesLoaded,
    @Default(false) bool isProductsLoaded,
  }) = _StreeplijstState;

  bool get isInitialized =>
      isCategoriesLoaded && isProductsLoaded;
}
