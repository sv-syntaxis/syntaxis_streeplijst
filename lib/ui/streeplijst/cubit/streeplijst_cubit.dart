import 'package:bloc/bloc.dart';

import '../../../core/entities/category/category.dart';
import '../../../core/entities/product/product.dart';
import '../../../core/providers/authentication/authentication_provider.dart';
import '../../../core/use_cases/authentication/logout_use_case.dart';
import '../../../core/use_cases/categories/get_categories_use_case.dart';
import '../../../core/use_cases/products/load_products_by_category_id_use_case.dart';
import 'streeplijst_state.dart';

export 'streeplijst_state.dart';

class StreeplijstCubit extends Cubit<StreeplijstState> {
  final LogoutUseCase _logoutUseCase;
  final GetCategoriesUseCase _getCategoriesUseCase;
  final LoadProductsByCategoryIdUseCase _loadProductsByCategoryIdUseCase;

  late final _productsPlaceholder;

  late final _categoryPlaceholder;

  final AuthenticationProvider _authenticationProvider;

  StreeplijstCubit(
    this._logoutUseCase,
    this._getCategoriesUseCase,
    this._loadProductsByCategoryIdUseCase,
    this._authenticationProvider,
  ) : super(StreeplijstState(
          categoryIndex: 0,
          balance: -(_authenticationProvider.user!.balance / 100),
        )) {
    _productsPlaceholder = List<Product>.generate(
      25,
      (i) => Product(
        id: i,
        price: i.toDouble(),
        discountPercentage: 0,
        name: 'a                    a',
        deposit: .15,
      ),
    );

    _categoryPlaceholder = List<Category>.generate(
      2,
      (i) => Category(name: 'a                    a', id: i),
    );

    emit(
      state.copyWith(
          categories: _categoryPlaceholder, products: _productsPlaceholder),
    );
  }

  Future<void> init() async {
    final categories = await _getCategoriesUseCase();
    emit(state.copyWith(categories: categories, isCategoriesLoaded: true));
    selectCategory(state.categoryIndex);
  }

  Future<void> logout() {
    return _logoutUseCase();
  }

  Future<void> selectCategory(int index) async {
    emit(state.copyWith(
      categoryIndex: index,
      isProductsLoaded: false,
      products: _productsPlaceholder,
    ));
    final products =
        await _loadProductsByCategoryIdUseCase(state.categories![index].id);
    emit(state.copyWith(products: products, isProductsLoaded: true));
  }
}
