import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:skeletonizer/skeletonizer.dart';

import '../cubit/streeplijst_cubit.dart';
import '../widgets/product_card.dart';
import '../widgets/shopping_basket/cubit/shopping_basket_cubit.dart';

class ProductOverview extends StatelessWidget {
  final _controller =
      ScrollController(keepScrollOffset: false, initialScrollOffset: 0);

  ProductOverview({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<StreeplijstCubit, StreeplijstState>(
      listener: (c, s) => _controller.jumpTo(0),
      builder: (context, state) {
        return Skeletonizer(
          enabled: !state.isProductsLoaded,
          child: Scrollbar(
            thumbVisibility: true,
            trackVisibility: true,
            controller: _controller,
            child: AlignedGridView.custom(
              shrinkWrap: true,
              controller: _controller,
              crossAxisSpacing: 32,
              mainAxisSpacing: 32,
              gridDelegate: SliverSimpleGridDelegateWithMaxCrossAxisExtent(
                maxCrossAxisExtent: 400,
              ),
              itemCount: state.products?.length ?? 0,
              padding: const EdgeInsets.all(16.0),
              itemBuilder: (context, index) {
                return ProductCard(
                  product: state.products![index],
                  onTap: () {
                    final cubit = context.read<ShoppingBasketCubit>();

                    if (cubit.getIndexOfProduct(state.products![index]) !=
                        null) {
                      cubit.addOneToProduct(state.products![index].id);
                    } else {
                      cubit.addProduct(state.products![index], 1);
                    }
                  },
                );
              },
            ),
          ),
        );
      },
    );
  }
}
