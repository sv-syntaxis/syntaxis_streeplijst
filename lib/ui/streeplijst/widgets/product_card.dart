import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:skeletonizer/skeletonizer.dart';

import '../../../core/entities/product/product.dart';
import '../../../utils/extensions.dart';
import 'add_to_card_widget.dart';

class ProductCard extends StatelessWidget {
  final Product product;
  final VoidCallback onTap;

  const ProductCard({
    Key? key,
    required this.product,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return IntrinsicHeight(
      child: Stack(
        clipBehavior: Clip.none,
        children: [
          Card(
            clipBehavior: Clip.antiAlias,
            // shape: RoundedRectangleBorder(
            //   borderRadius: BorderRadius.circular(16),
            // ),
            child: Skeleton.replace(
              replacement: _buildBones(theme),
              child: InkWell(
                onTap: onTap,
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Container(
                      height: 240,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: CachedNetworkImageProvider(
                            'http://placekitten.com/240/240',
                          ),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    Divider(
                      height: 10,
                      thickness: 1,
                      color:
                          (theme.colorScheme.primary as MaterialColor).shade200,
                    ),
                    Expanded(
                      child: Padding(
                        padding: EdgeInsets.only(left: 8, bottom: 16, right: 8),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Skeleton.leaf(
                                child: Text(
                                  product.name,
                                  maxLines: 2,
                                  style: theme.textTheme.bodyMedium,
                                ),
                              ),
                            ),
                            const SizedBox(
                              height: 8,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              mainAxisSize: MainAxisSize.max,
                              children: [
                                product.hasDeposit
                                    ? Skeleton.leaf(
                                        child: Text(
                                          'deposit'.tr(args: [
                                            (product.deposit / 100).toCurrency()
                                          ]),
                                          style: theme.textTheme.titleSmall,
                                        ),
                                      )
                                    : Expanded(child: const SizedBox.shrink()),
                                Text(
                                  '${(product.totalWithoutDiscount / 100).toCurrency()}',
                                  style: theme.textTheme.bodyMedium,
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                    Divider(
                      height: 1,
                      color:
                          (theme.colorScheme.primary as MaterialColor).shade200,
                    ),
                    Skeleton.keep(child: AddToCardWidget(product: product)),
                  ],
                ),
              ),
            ),
          ),
          if (product.hasDiscount)
            Positioned(
              top: -4,
              left: -6,
              child: Container(
                padding: const EdgeInsets.symmetric(horizontal: 6, vertical: 2),
                decoration: const BoxDecoration(
                  color: Colors.red,
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(8),
                    bottomRight: Radius.circular(8),
                  ),
                ),
                child: Text(
                  '- %${product.discountPercentage}',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 12,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
        ],
      ),
    );
  }

  Widget _buildBones(ThemeData theme) {
    return Skeletonizer.bones(
      child: Column(
        children: [
          Bone(height: 240, width: double.infinity),
          Divider(
            height: 10,
            thickness: 1,
            color: (theme.colorScheme.primary as MaterialColor).shade200,
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(left: 8, bottom: 16, right: 8),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Bone.text(
                      style: theme.textTheme.bodyMedium,
                      width: 150,
                    ),
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Bone.text(
                        style: theme.textTheme.titleSmall,
                        width: 75,
                      ),
                      Bone.text(
                        style: theme.textTheme.bodyMedium,
                        width: 50,
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
          Divider(
            height: 1,
            color: (theme.colorScheme.primary as MaterialColor).shade200,
          ),
          Skeleton.keep(child: AddToCardWidget(product: product)),
        ],
      ),
    );
  }
}
