import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../utils/extensions.dart';
import '../../../../shared/call_to_action_button.dart';
import '../../confirm_order/view/confirm_order_dialog.dart';
import '../../shopping_basket/cubit/shopping_basket_cubit.dart';
import '../../shopping_basket/cubit/shopping_basket_state.dart';
import 'shopping_basket_item_widget.dart';

class ShoppingBasketWidget extends StatelessWidget {
  final _controller = ScrollController();

  @override
  Widget build(BuildContext context) {
    final cubit = BlocProvider.of<ShoppingBasketCubit>(context);
    return Material(
      color: Colors.white,
      child: BlocBuilder<ShoppingBasketCubit, ShoppingBasketState>(
        bloc: cubit,
        builder: (context, state) => Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 32),
              child: CallToActionButton(
                text: 'checkout'.tr(),
                onPressed: state.shoppingBasketItems.isNotEmpty
                    ? () async {
                        final mediaQuery = MediaQuery.of(context);

                        final horizontal = (mediaQuery.size.width -
                                mediaQuery.size.width * 0.625) /
                            2;
                        final vertical = (mediaQuery.size.height -
                                mediaQuery.size.height * 0.711) /
                            2;

                        final shouldCheckout = await showDialog(
                          context: context,
                          builder: (dialogContext) => BlocProvider.value(
                            value: cubit,
                            child: Builder(
                              builder: (context) => BlocBuilder<
                                  ShoppingBasketCubit, ShoppingBasketState>(
                                builder: (context, state) => Dialog(
                                  insetPadding: EdgeInsets.symmetric(
                                    vertical: vertical,
                                    horizontal: horizontal,
                                  ),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(16),
                                  ),
                                  child: SizedBox(
                                    width: double.maxFinite,
                                    child: ConfirmOrderDialog(),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        );

                        if (shouldCheckout ?? false) {
                          BlocProvider.of<ShoppingBasketCubit>(context)
                              .checkout();
                        }
                      }
                    : null,
                backgroundColor: Theme.of(context).colorScheme.secondary,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 8),
              child: Text(
                '${(state.totalPrice / 100).toCurrency()}',
                style: Theme.of(context).textTheme.headlineLarge,
              ),
            ),
            if (state.totalDiscount != 0)
              Padding(
                padding: const EdgeInsets.only(bottom: 32),
                child: Text(
                  'your_discount'
                      .tr(args: [(state.totalDiscount / 100).toCurrency()]),
                  style: Theme.of(context).textTheme.caption,
                ),
              ),
            Expanded(
              child: Scrollbar(
                controller: _controller,
                trackVisibility: true,
                thumbVisibility: true,
                child: ListView.separated(
                  controller: _controller,
                  itemCount: state.shoppingBasketItems.length,
                  shrinkWrap: true,
                  itemBuilder: (_, index) => ShoppingBasketItemWidget(
                    product: state.shoppingBasketItems[index].product,
                    amount: state.shoppingBasketItems[index].amount,
                  ),
                  separatorBuilder: (_, index) => Divider(
                    color:
                        (Theme.of(context).colorScheme.primary as MaterialColor)
                            .shade200,
                    height: 16,
                    thickness: 1,
                    indent: 8,
                    endIndent: 8,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
