import 'package:equatable/equatable.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../../../core/entities/product/product.dart';
import '../../../../../core/entities/transaction_item/transaction_item.dart';

part 'shopping_basket_state.freezed.dart';

@Freezed(makeCollectionsUnmodifiable: false)
class ShoppingBasketState with _$ShoppingBasketState {
  const factory ShoppingBasketState({
    required List<ShoppingBasketItem> shoppingBasketItems,
    required double totalPrice,
    required double totalDiscount,
  }) = _ShoppingBasketState;
}

class ShoppingBasketItem extends Equatable {
  final int amount;
  final Product product;

  ShoppingBasketItem(this.product, this.amount);

  ShoppingBasketItem _copyWith({int? amount, Product? product}) {
    return ShoppingBasketItem(product ?? this.product, amount ?? this.amount);
  }

  ShoppingBasketItem increaseAmount() {
    return _copyWith(amount: amount + 1);
  }

  ShoppingBasketItem decreaseAmount() {
    return _copyWith(amount: amount - 1);
  }

  @override
  List<Object?> get props => [amount, product];
}

extension ShoppingBasketStateExtension on ShoppingBasketState {
  List<TransactionItemWrite> toTransactionItems() => shoppingBasketItems
      .map((e) => TransactionItemWrite(amount: e.amount, product: e.product.id))
      .toList();
}
