import 'package:collection/collection.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../core/entities/product/product.dart';
import 'shopping_basket/cubit/shopping_basket_cubit.dart';
import 'shopping_basket/cubit/shopping_basket_state.dart';

class AddToCardWidget extends StatelessWidget {
  final Product product;

  const AddToCardWidget({required this.product, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return SizedBox(
      height: 40,
      child: DecoratedBox(
        decoration: BoxDecoration(
          color: Theme.of(context).colorScheme.secondary,
        ),
        child: BlocBuilder<ShoppingBasketCubit, ShoppingBasketState>(
          builder: (context, state) {
            final item = state.shoppingBasketItems.singleWhereOrNull(
                (element) => element.product.id == product.id);

            if (item != null) {
              return Row(
                mainAxisSize: MainAxisSize.max,
                children: [
                  _IconButton(
                    onTap: () => context
                        .read<ShoppingBasketCubit>()
                        .removeOneFromProduct(product.id),
                    icon: Icons.remove,
                  ),
                  Expanded(
                    child: DecoratedBox(
                      decoration: BoxDecoration(color: Colors.white),
                      child: SizedBox(
                        height: 40,
                        child: Center(
                          child: Text(
                            'amount_in_cart'.tr(args: [item.amount.toString()]),
                            style: theme.textTheme.bodyMedium,
                            textAlign: TextAlign.start,
                          ),
                        ),
                      ),
                    ),
                  ),
                  _IconButton(
                    onTap: () => context
                        .read<ShoppingBasketCubit>()
                        .addOneToProduct(product.id),
                    icon: Icons.add,
                  ),
                ],
              );
            }
            return Center(
              child: Text(
                'add_to_cart'.tr(),
                style: theme.textTheme.bodyMedium!
                    .copyWith(color: theme.colorScheme.onSecondary),
              ),
            );
          },
        ),
      ),
    );
  }
}

class _IconButton extends StatelessWidget {
  final VoidCallback onTap;
  final IconData icon;

  const _IconButton({
    Key? key,
    required this.onTap,
    required this.icon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox.square(
      dimension: 40,
      child: Material(
        color: Theme.of(context).colorScheme.secondary,
        child: InkWell(
          onTap: onTap,
          child: Center(
            child: Icon(
              icon,
              color: Theme.of(context).colorScheme.onSecondary,
              size: 20,
            ),
          ),
        ),
      ),
    );
  }
}
