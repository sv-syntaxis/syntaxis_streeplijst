import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:go_router/go_router.dart';

import '../widgets/keyboard_popout.dart';

class LandingPage extends StatelessWidget {
  static const String pagePath = '/landing';

  static void route(BuildContext context) {
    context.go(pagePath);
  }

  const LandingPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Builder(
      builder: (context) {
        return Scaffold(
          body: Stack(
            children: [
              SizedBox.expand(
                child: Container(
                  width: 726,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: AssetImage(
                          'assets/images/landingpage_background.png'),
                    ),
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(height: 151),
                      SvgPicture.asset(
                        'assets/images/logo.svg',
                      ),
                      SizedBox(height: 92),
                      Text(
                        'scan_card'.tr(),
                        style: TextStyle(
                          height: 1.5,
                          color: Color.fromARGB(255, 119, 193, 69),
                          fontWeight: FontWeight.w500,
                          fontSize: 34,
                        ),
                      ),
                      SizedBox(height: 48),
                      Expanded(
                        child: Align(
                          alignment: Alignment.bottomCenter,
                          child: Padding(
                            padding: const EdgeInsets.only(bottom: 32),
                            child: Text(
                              'app_by'.tr(),
                              style: TextStyle(
                                height: 1.5,
                                color: Color.fromARGB(255, 211, 235, 194),
                                fontWeight: FontWeight.w300,
                                fontSize: 34,
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Align(
                  alignment: Alignment.topLeft,
                  child: KeyboardPopout(),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
