import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

import '../../streeplijst/views/streeplijst_page.dart';
import '../views/landing_page.dart';
import '../views/splash_screen.dart';
import 'cubit/login_cubit.dart';

class StreeplijstRouter {
 

  static GoRouter router (LoginCubit loginCubit) =>  GoRouter(
    routes: [
      GoRoute(
        path: SplashScreen.pagePath,
        pageBuilder: (context, state) =>
            MaterialPage(child: const SplashScreen()),
        redirect: (context, state) => LandingPage.pagePath,
      ),
      GoRoute(
        path: LandingPage.pagePath,
        pageBuilder: (context, state) =>
            MaterialPage(child: const LandingPage()),
      ),
      GoRoute(
        path: StreeplijstPage.pagePath,
        pageBuilder: (context, state) =>
            MaterialPage(child: const StreeplijstPage()),
      ),
    ],
    initialLocation: SplashScreen.pagePath,
    debugLogDiagnostics: kDebugMode,
    refreshListenable: GoRouterRefreshStream(loginCubit.stream),
    redirect: (context, goRouterState) async {
      final publicPages = <String>[
        SplashScreen.pagePath,
        LandingPage.pagePath,
      ];

      return loginCubit.state.when(
        loggedOut: () {
          if (publicPages.any((e) => e.contains(goRouterState.matchedLocation))) {
            // The user is allowed to be on this page without
            // authentication.
            debugPrint('logged out, public page');
          } else {
            // User not allowed to be on page, redirect to login.
            debugPrint('logged out, private page, redirect to login');
            return LandingPage.pagePath;
          }
        },
        loggedIn: () {
          return StreeplijstPage.pagePath;
        },
      );
    },
  );
}

/// Converts a [Stream] into a [Listenable]
///
/// {@tool snippet}
/// Typical usage is as follows:
///
/// ```dart
/// GoRouter(
///  refreshListenable: GoRouterRefreshStream(stream),
/// );
/// ```
/// {@end-tool}
class GoRouterRefreshStream extends ChangeNotifier {
  /// Creates a [GoRouterRefreshStream].
  ///
  /// Every time the [stream] receives an event the [GoRouter] will refresh its
  /// current route.
  GoRouterRefreshStream(Stream<dynamic> stream) {
    notifyListeners();
    _subscription = stream.asBroadcastStream().listen(
          (dynamic _) => notifyListeners(),
        );
  }

  late final StreamSubscription<dynamic> _subscription;

  @override
  void dispose() {
    _subscription.cancel();
    super.dispose();
  }
}
