import 'package:freezed_annotation/freezed_annotation.dart';

part 'login_state.freezed.dart';

@freezed
class LoginState with _$LoginState {
  factory LoginState.loggedIn() = _LoggedIn;

  factory LoginState.loggedOut() = _LoggedOut;
}
