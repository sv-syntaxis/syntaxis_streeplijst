import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:syntaxis_streeplijst/core/use_cases/authentication/login_use_case.dart';

import './login_state.dart';
import '../../../../core/entities/user/user.dart';
import '../../../../core/use_cases/start/app_started_use_case.dart';

class LoginCubit extends Cubit<LoginState> {
  final AppStartedUseCase _appStartedUseCase;
  final LoginUseCase _loginUseCase;
  late final StreamSubscription authSub;
  User? user;

  bool get isAuthenticated => user != null;

  LoginCubit(this._appStartedUseCase, this._loginUseCase)
      : super(LoginState.loggedOut()) {
    init();
  }

  void init() async {
    authSub = _appStartedUseCase.authStatus.listen((user) {
      this.user = user;

      if (user == null) {
        emit(LoginState.loggedOut());
      } else {
        emit(LoginState.loggedIn());
      }
    });
  }

  Future<void> loginWithBarcode(String barcode) async {
    _loginUseCase.call(barcode);
  }
}
