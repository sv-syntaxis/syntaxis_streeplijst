import '../../../core/entities/category/category.dart';
import '../../../core/repositories/api_repository/api_repository.dart';
import '../../../core/repositories/categories/categories_repository.dart';
import '../../api_client.dart';

class ApiCategoryRepository extends ApiRepository with CategoriesRepository {
  ApiCategoryRepository(ApiClient client) : super(client);

  @override
  Future<List<Category>> getCategories() async {
    final pageable = await client.getCategories();

    return pageable.content.map(Category.fromJson).toList();
  }
}
