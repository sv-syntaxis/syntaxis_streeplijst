import '../../../core/entities/transaction/transaction.dart';
import '../../../core/repositories/api_repository/api_repository.dart';
import '../../../core/repositories/transaction/transaction_repository.dart';
import '../../api_client.dart';


class ApiTransactionsRepository extends ApiRepository with TransactionsRepository {
  ApiTransactionsRepository(ApiClient client) : super(client);

  @override
  Future<void> addTransaction(TransactionWrite transaction) async  {
    await client.createTransaction(transaction);
  }
}
