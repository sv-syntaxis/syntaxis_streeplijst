import '../../../core/entities/token/token.dart';
import '../../../core/repositories/api_repository/api_repository.dart';
import '../../../core/repositories/login/login_repository.dart';
import '../../api_client.dart';

class ApiLoginRepository extends ApiRepository implements LoginRepository {
  ApiLoginRepository(ApiClient client) : super(client);

  @override
  Future<Token?> login(String barcode) {
    return client.authenticateStreeplijst(LoginRequest(token: barcode));
  }
}
