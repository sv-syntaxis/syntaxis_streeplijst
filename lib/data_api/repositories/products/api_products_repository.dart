import '../../../core/entities/product/product.dart';
import '../../../core/repositories/api_repository/api_repository.dart';
import '../../../core/repositories/products/products_repository.dart';
import '../../api_client.dart';

class ApiProductsRepository extends ApiRepository with ProductsRepository {
  ApiProductsRepository(ApiClient client) : super(client);

  @override
  Future<List<Product>> getProductByCategoryId(int categoryId) async {
    final pageable = await client.getProducts(categoryId);

    return pageable.content.map(Product.fromJson).toList();
  }

  @override
  Future<List<Product>> getProducts() async {
    final pageable = await client.getProducts(null);

    return pageable.content.map(Product.fromJson).toList();
  }
}
